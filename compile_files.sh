#!/bin/bash

for i in {1..3}; do
find . -type f -name *.tex -exec pdflatex '{}' \;
done;

find . -type f ! -name *.pdf -delete
